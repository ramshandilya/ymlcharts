//
//  ViewController.m
//  YMLCharts
//
//  Created by Ramsundar Shandilya on 10/1/14.
//  Copyright (c) 2014 Ramsundar Shandilya. All rights reserved.
//

#import "ViewController.h"
#import "APCLineGraphView.h"
#import "APCDiscreteGraphView.h"
#import "APCNormalDistributionGraphView.h"
#import "APCBarGraphView.h"

@interface ViewController () <APCLineGraphViewDataSource, APCDiscreteGraphViewDataSource, APCBaseGraphViewDelegate, APCBarGraphViewDataSource>

@property (weak, nonatomic) IBOutlet APCLineGraphView *lineGraphView;
@property (weak, nonatomic) IBOutlet APCNormalDistributionGraphView *normalGraphView;
@property (weak, nonatomic) IBOutlet APCDiscreteGraphView *discreteGraphView;
@property (weak, nonatomic) IBOutlet APCBarGraphView *barGraphView;
@property (weak, nonatomic) IBOutlet UISwitch *compareSwitch;
@property (weak, nonatomic) IBOutlet UISegmentedControl *segmentedControl;

@end

@implementation ViewController

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    self.segmentedControl.tintColor = [UIColor clearColor];
    [self.segmentedControl setTitleTextAttributes:@{NSFontAttributeName: [UIFont systemFontOfSize:15.0], NSForegroundColorAttributeName:[UIColor grayColor]} forState:UIControlStateNormal];
    [self.segmentedControl setTitleTextAttributes:@{NSFontAttributeName: [UIFont systemFontOfSize:17.0], NSForegroundColorAttributeName:[UIColor blackColor]} forState:UIControlStateSelected];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    self.lineGraphView.delegate = self;
    self.normalGraphView.delegate = self;
    self.discreteGraphView.delegate = self;
    self.barGraphView.delegate = self;
    
    [self.lineGraphView refreshGraph];
    [self.discreteGraphView refreshGraph];
    [self.normalGraphView refreshGraph];
    [self.barGraphView refreshGraph];
}

# pragma mark - APCLineCharViewDataSource

- (NSInteger)lineGraph:(APCLineGraphView *)graphView numberOfPointsInPlot:(NSInteger)plotIndex
{
    return 7;
}

- (NSInteger)numberOfPlotsInLineGraph:(APCLineGraphView *)graphView
{
    return 1;//self.compareSwitch.isOn ? 2 : 1;
}

- (CGFloat)lineGraph:(APCLineGraphView *)graphView plot:(NSInteger)plotIndex valueForPointAtIndex:(NSInteger)pointIndex
{
    CGFloat value;
    
    if (plotIndex == 0) {
//        NSArray *values = @[@(NSNotFound), @(NSNotFound), @(NSNotFound), @(24.0), @(NSNotFound)]; // Single Point
//        NSArray *values = @[@(NSNotFound), @(36.0), @(NSNotFound), @(NSNotFound), @24.0]; //Two Points
//        NSArray *values = @[@(NSNotFound), @(36.0), @(48.0), @(NSNotFound), @24.0]; //3 Points
//        NSArray *values = @[@(67.0), @(36.0), @(48.0), @(NSNotFound), @24.0]; //4 Points
//        NSArray *values = @[@(67.0), @(36.0), @(48.0), @(NSNotFound), @(NSNotFound)]; //3 Points
//        NSArray *values = @[@(22), @(10), @(27), @(27), @(64),@(80)];
        NSArray *values = @[@(2.81387),
                            @(34.2800),
                            @(153.632),
                            @(253.297),
                            @(153.632),
                            @(34.2800),
                            @(2.81387)];
        value = ((NSNumber *)values[pointIndex]).floatValue;
    } else {
        NSArray *values = @[@23.0, @46.0, @87.0, @12.0, @51.0, @66.0];
        value = ((NSNumber *)values[pointIndex]).floatValue;
    }
    
    return value;
    
}

- (void)lineGraph:(APCLineGraphView *)graphView touchesMovedToXPosition:(CGFloat)xPosition
{
    [self.discreteGraphView scrubReferenceLineForXPosition:xPosition];
}



- (NSString *)lineGraph:(APCLineGraphView *)graphView titleForXAxisAtIndex:(NSInteger)pointIndex
{
    return @"Sep";
}

//--------------------------------

- (NSInteger)numberOfPlotsInDiscreteGraph:(APCDiscreteGraphView *)graphView
{
    return self.compareSwitch.isOn ? 2 : 1;
}

- (NSInteger)discreteGraph:(APCDiscreteGraphView *)graphView numberOfPointsInPlot:(NSInteger)plotIndex
{
    return 6;
}

- (APCRangePoint *)discreteGraph:(APCDiscreteGraphView *)graphView plot:(NSInteger)plotIndex valueForPointAtIndex:(NSInteger)pointIndex
{
    APCRangePoint *rangePoint = [APCRangePoint new];
    
    if (plotIndex == 0) {
        NSArray *minValues = @[@(22), @(27), @(48), @(10), @(NSNotFound),@(80)];
        NSArray *maxValues = @[@(76), @(36), @(78), @(20), @(NSNotFound),@(80)];
        
        rangePoint.minimumValue = [minValues[pointIndex] floatValue];
        rangePoint.maximumValue = [maxValues[pointIndex] floatValue];
    } else {
        NSArray *minValues = @[@(18), @(17), @(58), @(10), @(10), @(60)];
        NSArray *maxValues = @[@(64), @(36), @(88), @(20), @(43), @(60)];
        
        rangePoint.minimumValue = [minValues[pointIndex] floatValue];
        rangePoint.maximumValue = [maxValues[pointIndex] floatValue];
    }
    
    
    return rangePoint;
}


- (void)discreteGraph:(APCDiscreteGraphView *)graphView touchesMovedToXPosition:(CGFloat)xPosition
{
    [self.lineGraphView scrubReferenceLineForXPosition:xPosition];
}

//--------------------------------

# pragma mark - APCBarCharViewDataSource

- (NSInteger)barGraph:(APCLineGraphView *)graphView numberOfPointsInPlot:(NSInteger)plotIndex
{
    return 6;
}

- (NSInteger)numberOfPlotsInBarGraph:(APCLineGraphView *)graphView
{
    return 1;//self.compareSwitch.isOn ? 2 : 1;
}

- (APCStackedDataPoint *)barGraph:(APCLineGraphView *)graphView plot:(NSInteger)plotIndex valueForPointAtIndex:(NSInteger)pointIndex
{
    NSInteger random1 = arc4random() % 70;
    NSInteger random2 = arc4random() % 20;
    NSInteger random3 = arc4random() % 10;
    
    APCStackedDataPoint *stackedData = [[APCStackedDataPoint alloc] initWithStackedValues:@[@(random1), @(random3), @(random2)]];
    
    return stackedData;
    
}

- (CGFloat)maximumValueForBarGraph:(APCBarGraphView *)graphView
{
    return 100;
}

- (CGFloat)minimumValueForBarGraph:(APCBarGraphView *)graphView
{
    return 0;
}

- (void)barGraph:(APCLineGraphView *)graphView touchesMovedToXPosition:(CGFloat)xPosition
{
    [self.discreteGraphView scrubReferenceLineForXPosition:xPosition];
}



- (NSString *)barGraph:(APCLineGraphView *)graphView titleForXAxisAtIndex:(NSInteger)pointIndex
{
    return @"Sep";
}

- (UIColor *)barGraph:(APCBarGraphView *)graphView fillColorForStackedLayerAtIndex:(NSInteger)layerIndex
{
    NSArray *colorArray = @[[UIColor colorWithRed:244/255.f green:190/255.f blue:74/255.f alpha:1.f],
                            [UIColor colorWithRed:0.957 green:0.305 blue:0.424 alpha:1.000],
                            [UIColor colorWithRed:0.516 green:0.957 blue:0.688 alpha:1.000]];
    
    return colorArray[layerIndex];
}

- (void)graphView:(APCBaseGraphView *)graphView touchesMovedToXPosition:(CGFloat)xPosition
{
    if ([graphView isKindOfClass:[APCLineGraphView class]]) {
        [self.discreteGraphView scrubReferenceLineForXPosition:xPosition];
    } else {
        [self.lineGraphView scrubReferenceLineForXPosition:xPosition];
    }
}


- (IBAction)compare:(UISwitch *)sender
{
    [self.lineGraphView refreshGraph];
    [self.discreteGraphView refreshGraph];
}

@end
